<?php
    class Htmalpage{
        protected $title = "Title - Example";
        protected $body = "Body - Example";
        function __construct($title = "",$body = ""){
            if($title != ""){
                $this->title = $title;
            }
            if($body != ""){
                $this->body = $body;
            }
        }
        public function view(){
            echo "
                <html>
                <head>
                    <title>$this->title</title>
                </head>
                <body>
                    $this->body
                </body>
                </html>
                ";
        }
        
    }


class coloredHtml extends Htmalpage {
        protected $color = 'red';
        protected $colors = array('red','yellow','green','purple','blue');
        public function __set($property,$value){
            if ($property == 'color'){
                if(in_array($value,$this->colors)){
                    $this->color=$value;
                }
                else{
                    die("Error! Enter A New Color");    
                }
            }
        }
        public function view(){
            echo "
                <html>
                <head>
                    <title>$this->title</title>
                </head>
                <body>
                <p style = 'color:$this->color'>
                    $this->body
                </p>
                </body>
                </html>
                ";
        }
}

class fontsizeHtml extends coloredHtml{
    protected $size;
    public function __set($property,$value){
        if ($property == 'size'){
            $size = array('10px','11px','12px','13px','14px','15px','16px','17px','18px','19px','20px','21px','22px','23px','24px');
            if(in_array($value, $size)){
                $this->size = $value;          
            }
            else{
                die("Error! Enter A New Font Size ");        
            }
        }
        elseif($property == 'color'){
            parent::__set($property,$value);
        }
    }
     
    public function view(){
        echo "<html>
              <head>
                <title>$this->title</title> 
              </head>
              <body>
                <p style = 'color:$this->color; font-size:$this->size;' >$this->body</p>
              </body>
              </html>    
              </p>";       
    }            
}


?>

